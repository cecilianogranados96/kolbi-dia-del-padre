﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace kolbiinteractiva
{
    public partial class Padre : Form
    {
        public static String nombrepadre;
        public static String sunombre;

        public Padre()
        {
            InitializeComponent();
        }

        private void padre_Load(object sender, EventArgs e)
        {
            this.TopMost = true;
            this.FormBorderStyle = FormBorderStyle.None;
            this.WindowState = FormWindowState.Maximized;
        }

        private void btnnext2_Click(object sender, EventArgs e)
        {
            nombrepadre = txtnombrepapa.Text;
            sunombre = txtsunombre.Text;
            if ((nombrepadre == "" && sunombre == "") || (nombrepadre.Length > 14 && sunombre.Length > 14))
            {
                lblalerta.Visible = true;


            }
            else
            {
                this.Hide();
                Frase fe = new Frase();
                fe.Show();

            }

        }

        private void lblsunombre_Click(object sender, EventArgs e)
        {

        }
        private void txtnombrepapa_GotFocus(object sender, EventArgs e) {
            System.Diagnostics.Process.Start("osk.exe");
        }


    }
}
