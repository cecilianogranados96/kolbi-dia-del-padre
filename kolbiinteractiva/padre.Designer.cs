﻿namespace kolbiinteractiva
{
    partial class Padre
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblnombrepadre = new System.Windows.Forms.Label();
            this.txtnombrepapa = new System.Windows.Forms.TextBox();
            this.lblsunombre = new System.Windows.Forms.Label();
            this.txtsunombre = new System.Windows.Forms.TextBox();
            this.btnnext2 = new System.Windows.Forms.Button();
            this.lblalerta = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblnombrepadre
            // 
            this.lblnombrepadre.AutoSize = true;
            this.lblnombrepadre.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblnombrepadre.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(123)))));
            this.lblnombrepadre.Location = new System.Drawing.Point(315, 21);
            this.lblnombrepadre.Name = "lblnombrepadre";
            this.lblnombrepadre.Size = new System.Drawing.Size(884, 73);
            this.lblnombrepadre.TabIndex = 0;
            this.lblnombrepadre.Text = "Ingrese el nombre de su papá";
            // 
            // txtnombrepapa
            // 
            this.txtnombrepapa.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnombrepapa.Location = new System.Drawing.Point(328, 115);
            this.txtnombrepapa.Name = "txtnombrepapa";
            this.txtnombrepapa.Size = new System.Drawing.Size(743, 47);
            this.txtnombrepapa.TabIndex = 1;
            // 
            // lblsunombre
            // 
            this.lblsunombre.AutoSize = true;
            this.lblsunombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblsunombre.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(123)))));
            this.lblsunombre.Location = new System.Drawing.Point(418, 207);
            this.lblsunombre.Name = "lblsunombre";
            this.lblsunombre.Size = new System.Drawing.Size(567, 73);
            this.lblsunombre.TabIndex = 2;
            this.lblsunombre.Text = "Ingrese su nombre";
            this.lblsunombre.Click += new System.EventHandler(this.lblsunombre_Click);
            // 
            // txtsunombre
            // 
            this.txtsunombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsunombre.Location = new System.Drawing.Point(328, 322);
            this.txtsunombre.Name = "txtsunombre";
            this.txtsunombre.Size = new System.Drawing.Size(743, 47);
            this.txtsunombre.TabIndex = 3;
            // 
            // btnnext2
            // 
            this.btnnext2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnnext2.AutoSize = true;
            this.btnnext2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(147)))), ((int)(((byte)(142)))));
            this.btnnext2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnnext2.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnnext2.ForeColor = System.Drawing.Color.White;
            this.btnnext2.Location = new System.Drawing.Point(974, 480);
            this.btnnext2.Name = "btnnext2";
            this.btnnext2.Size = new System.Drawing.Size(243, 78);
            this.btnnext2.TabIndex = 5;
            this.btnnext2.Text = "Siguiente";
            this.btnnext2.UseVisualStyleBackColor = false;
            this.btnnext2.Click += new System.EventHandler(this.btnnext2_Click);
            // 
            // lblalerta
            // 
            this.lblalerta.AutoSize = true;
            this.lblalerta.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblalerta.ForeColor = System.Drawing.Color.Red;
            this.lblalerta.Location = new System.Drawing.Point(45, 480);
            this.lblalerta.Name = "lblalerta";
            this.lblalerta.Size = new System.Drawing.Size(885, 39);
            this.lblalerta.TabIndex = 6;
            this.lblalerta.Text = "Debe ingresar todos los datos o su nombre es muy largo";
            this.lblalerta.Visible = false;
            // 
            // Padre
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(214)))), ((int)(((byte)(58)))));
            this.ClientSize = new System.Drawing.Size(1246, 581);
            this.Controls.Add(this.lblalerta);
            this.Controls.Add(this.btnnext2);
            this.Controls.Add(this.txtsunombre);
            this.Controls.Add(this.lblsunombre);
            this.Controls.Add(this.txtnombrepapa);
            this.Controls.Add(this.lblnombrepadre);
            this.Name = "Padre";
            this.Text = "padre";
            this.Load += new System.EventHandler(this.padre_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblnombrepadre;
        private System.Windows.Forms.TextBox txtnombrepapa;
        private System.Windows.Forms.Label lblsunombre;
        private System.Windows.Forms.TextBox txtsunombre;
        private System.Windows.Forms.Button btnnext2;
        private System.Windows.Forms.Label lblalerta;
    }
}