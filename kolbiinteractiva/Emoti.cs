﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace kolbiinteractiva
{
    public partial class Emoti : Form
    {
        public static Bitmap emoescogido;
        public static Bitmap ultimaimagen = Frase.last;
        public static Bitmap toprint;
        public static Bitmap last;
        public static int contador_emo = 0;
        public Emoti()
        {
            InitializeComponent();
        }
        public void dibujar_imagen(Bitmap imagen)
        {
            Bitmap img;
            Graphics frase;
            if (contador_emo == 3 || contador_emo == 2)
            {
                img = new Bitmap(last);
                frase = Graphics.FromImage(img);
            }
            else {
                ultimaimagen = Frase.last;
                img = new Bitmap(ultimaimagen);
                frase = Graphics.FromImage(img);
                
            }
            switch (contador_emo)
            {
                case 1:
                    frase.DrawImage(emoescogido, 15, 350);
                    pictureBox1.Image = img;
                    last = new Bitmap(pictureBox1.Image);
                    break;
                case 2:
                    frase.DrawImage(emoescogido, 650, 430);
                    pictureBox1.Image = img;
                    last = new Bitmap(pictureBox1.Image);
                    break;
                case 3:
                    frase.DrawImage(emoescogido, 650, 250);
                    pictureBox1.Image = img;
                    last = new Bitmap(pictureBox1.Image);
                    break;
                case 4:
                    lblalerta.Visible = true;
                    lblalerta.Text = "Lo sentimos, ha excedido la capacidad de emoticones, presione siguiente para imprimir";
                    break;

            }

            
        }
        private void Emoti_Load(object sender, EventArgs e)
        {
            this.TopMost = true;
            this.FormBorderStyle = FormBorderStyle.None;
            this.WindowState = FormWindowState.Maximized;
            pictureBox1.Image = Frase.last;
            
        }

        private void btnnext4_Click(object sender, EventArgs e)
        {
            if (contador_emo < 3)
            {

                label2.Visible = true;
            }
            else
            {
                this.Hide();
                Print pt = new Print();
                pt.Show();
            }
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            contador_emo += 1;
            emoescogido = new Bitmap(pictureBox5.Image);
            dibujar_imagen(emoescogido);

            
            

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            contador_emo += 1;
            emoescogido = new Bitmap(pictureBox2.Image);
            dibujar_imagen(emoescogido);
            
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            contador_emo += 1;
            emoescogido = new Bitmap(pictureBox8.Image);
            dibujar_imagen(emoescogido);
           
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            contador_emo += 1;
            emoescogido = new Bitmap(pictureBox4.Image);
            dibujar_imagen(emoescogido);
        }

        private void pictureBox17_Click(object sender, EventArgs e)
        {
            contador_emo += 1;
            emoescogido = new Bitmap(pictureBox17.Image);
            dibujar_imagen(emoescogido);
        }

        private void pictureBox15_Click(object sender, EventArgs e)
        {
            contador_emo += 1;
            emoescogido = new Bitmap(pictureBox15.Image);
            dibujar_imagen(emoescogido);
        }

        private void pictureBox14_Click(object sender, EventArgs e)
        {
            contador_emo += 1;
            emoescogido = new Bitmap(pictureBox14.Image);
            dibujar_imagen(emoescogido);
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            contador_emo += 1;
            emoescogido = new Bitmap(pictureBox3.Image);
            dibujar_imagen(emoescogido);
        }

        private void pictureBox11_Click(object sender, EventArgs e)
        {
            contador_emo += 1;
            emoescogido = new Bitmap(pictureBox11.Image);
            dibujar_imagen(emoescogido);
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            contador_emo += 1;
            emoescogido = new Bitmap(pictureBox6.Image);
            dibujar_imagen(emoescogido);
        }

        private void pictureBox16_Click(object sender, EventArgs e)
        {
            contador_emo += 1;
            emoescogido = new Bitmap(pictureBox16.Image);
            dibujar_imagen(emoescogido);
        }

        private void pictureBox12_Click(object sender, EventArgs e)
        {
            contador_emo += 1;
            emoescogido = new Bitmap(pictureBox12.Image);
            dibujar_imagen(emoescogido);
        }

        private void pictureBox9_Click(object sender, EventArgs e)
        {
            contador_emo += 1;
            emoescogido = new Bitmap(pictureBox9.Image);
            dibujar_imagen(emoescogido);
        }

        private void pictureBox10_Click(object sender, EventArgs e)
        {
            contador_emo += 1;
            emoescogido = new Bitmap(pictureBox10.Image);
            dibujar_imagen(emoescogido);
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            contador_emo += 1;
            emoescogido = new Bitmap(pictureBox7.Image);
            dibujar_imagen(emoescogido);
        }

        private void pictureBox13_Click(object sender, EventArgs e)
        {
            contador_emo += 1;
            emoescogido = new Bitmap(pictureBox13.Image);
            dibujar_imagen(emoescogido);
        }
    }
}
