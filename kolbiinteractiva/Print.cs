﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Printing;
namespace kolbiinteractiva
{
    public partial class Print : Form
    {
        public Print()
        {
            InitializeComponent();
        }

        private void Print_Load(object sender, EventArgs e)
        {
            this.TopMost = true;
            this.FormBorderStyle = FormBorderStyle.None;
            this.WindowState = FormWindowState.Maximized;
            pictureBox2.Image = Emoti.last;
        }
        string GetDefaultPrinter()
        {
            PrinterSettings settings = new PrinterSettings();
            foreach (string printer in PrinterSettings.InstalledPrinters)
            {
                settings.PrinterName = printer;
                if (settings.IsDefaultPrinter)
                    return printer;
            }
            return string.Empty;
        }
        private void btnnext4_Click(object sender, EventArgs e)
        {
            
            
            PrintDocument img = new PrintDocument();
            img.DefaultPageSettings.Landscape = true;
            img.PrintPage += this.Doc_PrintPage;
            img.PrinterSettings.PrinterName = GetDefaultPrinter();
            try
            {
                img.Print();
                
            }
            finally {
                this.Close();
                Frase fe = new Frase();
                fe.Close();
                Emoti ei = new Emoti();
                ei.Close();
                Padre pe = new Padre();
                pe.Close();
                Form1 form = new Form1();
              
                form.Show();
                Emoti.contador_emo = 0;
                Emoti.ultimaimagen.Dispose();
            
            }
         


        }
        private void Doc_PrintPage(object sender, PrintPageEventArgs e)
        {
            Font font = new Font("Arial", 30);

            float x = 150;
            float y = 60;

            float lineHeight = font.GetHeight(e.Graphics);

           


            e.Graphics.DrawImage(pictureBox2.Image, x, y);
            

        }
    }
}
