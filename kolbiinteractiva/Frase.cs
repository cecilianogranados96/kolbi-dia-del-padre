﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace kolbiinteractiva
{
    public partial class Frase : Form
    {
        public static Bitmap imgescogida;
        public static Bitmap ultimaimagen;
        public static Bitmap last;
        public int escogida = 0;
        public Frase()
        {
            InitializeComponent();
            
        }

        private void Frase_Load(object sender, EventArgs e)
        {
            
            Padre pe = new Padre();
            String nombrepadre = Padre.nombrepadre;
            String sunombre = Padre.sunombre;
            this.TopMost = true;
            this.FormBorderStyle = FormBorderStyle.None;
            this.WindowState = FormWindowState.Maximized;
            Bitmap cartelpadre = new Bitmap(kolbiinteractiva.Properties.Resources.image002);
            Graphics textos = Graphics.FromImage(cartelpadre);
            FontFamily fontfpadre = new FontFamily("Microsoft Sans Serif");
            System.Drawing.Font fontpadre = new Font(fontfpadre, 20.0f, FontStyle.Bold);
            Font fontsunombre = new Font(fontfpadre, 20.0f, FontStyle.Bold);
            textos.DrawString(nombrepadre, fontpadre, Brushes.Green, 550, 90);
            textos.DrawString(sunombre, fontpadre, Brushes.Green, 550, 151);
            pictureBox15.Image = cartelpadre;
            ultimaimagen = new Bitmap(pictureBox15.Image);
        }
        public void dibujar_imagen(Bitmap imagen) {
            Bitmap img;
            Graphics frase;

            if(escogida == 3 || escogida == 2) {
                img = new Bitmap(last);
                frase = Graphics.FromImage(img);
            }else{
                img = new Bitmap(ultimaimagen);
                frase = Graphics.FromImage(img);
            }
            switch(escogida)
            {
                case 1:
                    
                    frase.DrawImage(imgescogida, 100, 450);
                    pictureBox15.Image = img;
                    last = new Bitmap(pictureBox15.Image);
                    break;
                case 2:
                    frase.DrawImage(imgescogida, 170, 350);
                    pictureBox15.Image = img;
                    last = new Bitmap(pictureBox15.Image);
                    break;
                case 3:
                    frase.DrawImage(imgescogida, 160, 250);
                    pictureBox15.Image = img;
                    last = new Bitmap(pictureBox15.Image);
                    break;
                case 4:
                    label2.Visible = true;
                    label2.Text = "Lo sentimos ha excedido la capacidad de frasess";
                    break;
                
            }
            
         
        }
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            imgescogida = new Bitmap(pictureBox1.Image);
            escogida += 1;
            dibujar_imagen(imgescogida);
            /*pictureBox2.Visible = false;
            pictureBox3.Visible = false;
            pictureBox4.Visible = false;
            pictureBox5.Visible = false;
            pictureBox6.Visible = false;
            pictureBox7.Visible = false;
            pictureBox8.Visible = false;
            pictureBox9.Visible = false;
            pictureBox10.Visible = false;
            pictureBox11.Visible = false;
            pictureBox12.Visible = false;
            pictureBox13.Visible = false;
            pictureBox13.Visible = false;
            pictureBox14.Visible = false;*/
        }

        private void pictureBox11_Click(object sender, EventArgs e)
        {
            escogida += 1;
            imgescogida = new Bitmap(pictureBox11.Image);
            dibujar_imagen(imgescogida);
           /* pictureBox2.Visible = false;
            pictureBox3.Visible = false;
            pictureBox4.Visible = false;
            pictureBox5.Visible = false;
            pictureBox6.Visible = false;
            pictureBox7.Visible = false;
            pictureBox8.Visible = false;
            pictureBox9.Visible = false;
            pictureBox10.Visible = false;
            pictureBox1.Visible = false;
            pictureBox12.Visible = false;
            pictureBox13.Visible = false;
            pictureBox13.Visible = false;
            pictureBox14.Visible = false;*/
        }

        private void pictureBox12_Click(object sender, EventArgs e)
        {
            escogida += 1;
            imgescogida = new Bitmap(pictureBox12.Image);
            dibujar_imagen(imgescogida);
            /*pictureBox2.Visible = false;
            pictureBox3.Visible = false;
            pictureBox4.Visible = false;
            pictureBox5.Visible = false;
            pictureBox6.Visible = false;
            pictureBox7.Visible = false;
            pictureBox8.Visible = false;
            pictureBox9.Visible = false;
            pictureBox10.Visible = false;
            pictureBox11.Visible = false;
            pictureBox1.Visible = false;
            pictureBox13.Visible = false;
            pictureBox13.Visible = false;
            pictureBox14.Visible = false;*/
        }

        private void pictureBox14_Click(object sender, EventArgs e)
        {
            escogida += 1;
            imgescogida = new Bitmap(pictureBox14.Image);
            dibujar_imagen(imgescogida);
            /*pictureBox2.Visible = false;
            pictureBox3.Visible = false;
            pictureBox4.Visible = false;
            pictureBox5.Visible = false;
            pictureBox6.Visible = false;
            pictureBox7.Visible = false;
            pictureBox8.Visible = false;
            pictureBox9.Visible = false;
            pictureBox10.Visible = false;
            pictureBox11.Visible = false;
            pictureBox12.Visible = false;
            pictureBox13.Visible = false;
            pictureBox13.Visible = false;
            pictureBox1.Visible = false;*/
           
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            escogida += 1;
            imgescogida = new Bitmap(pictureBox6.Image);
            dibujar_imagen(imgescogida);
            /*pictureBox2.Visible = false;
            pictureBox3.Visible = false;
            pictureBox4.Visible = false;
            pictureBox5.Visible = false;
            pictureBox1.Visible = false;
            pictureBox7.Visible = false;
            pictureBox8.Visible = false;
            pictureBox9.Visible = false;
            pictureBox10.Visible = false;
            pictureBox11.Visible = false;
            pictureBox12.Visible = false;
            pictureBox13.Visible = false;
            pictureBox13.Visible = false;
            pictureBox14.Visible = false;*/
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            escogida += 1;
            imgescogida = new Bitmap(pictureBox3.Image);
            dibujar_imagen(imgescogida);
            /*pictureBox2.Visible = false;
            pictureBox1.Visible = false;
            pictureBox4.Visible = false;
            pictureBox5.Visible = false;
            pictureBox6.Visible = false;
            pictureBox7.Visible = false;
            pictureBox8.Visible = false;
            pictureBox9.Visible = false;
            pictureBox10.Visible = false;
            pictureBox11.Visible = false;
            pictureBox12.Visible = false;
            pictureBox13.Visible = false;
            pictureBox13.Visible = false;
            pictureBox14.Visible = false;*/
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            escogida += 1;
            imgescogida = new Bitmap(pictureBox2.Image);
            dibujar_imagen(imgescogida);
            /*pictureBox1.Visible = false;
            pictureBox3.Visible = false;
            pictureBox4.Visible = false;
            pictureBox5.Visible = false;
            pictureBox6.Visible = false;
            pictureBox7.Visible = false;
            pictureBox8.Visible = false;
            pictureBox9.Visible = false;
            pictureBox10.Visible = false;
            pictureBox11.Visible = false;
            pictureBox12.Visible = false;
            pictureBox13.Visible = false;
            pictureBox13.Visible = false;
            pictureBox14.Visible = false;¨*/
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            imgescogida = new Bitmap(pictureBox4.Image);
            escogida += 1;
            dibujar_imagen(imgescogida);
           /* pictureBox2.Visible = false;
            pictureBox3.Visible = false;
            pictureBox1.Visible = false;
            pictureBox5.Visible = false;
            pictureBox6.Visible = false;
            pictureBox7.Visible = false;
            pictureBox8.Visible = false;
            pictureBox9.Visible = false;
            pictureBox10.Visible = false;
            pictureBox11.Visible = false;
            pictureBox12.Visible = false;
            pictureBox13.Visible = false;
            pictureBox13.Visible = false;
            pictureBox14.Visible = false;*/
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            imgescogida = new Bitmap(pictureBox8.Image);
            escogida += 1;
            dibujar_imagen(imgescogida);
           /* pictureBox2.Visible = false;
            pictureBox3.Visible = false;
            pictureBox4.Visible = false;
            pictureBox5.Visible = false;
            pictureBox6.Visible = false;
            pictureBox7.Visible = false;
            pictureBox1.Visible = false;
            pictureBox9.Visible = false;
            pictureBox10.Visible = false;
            pictureBox11.Visible = false;
            pictureBox12.Visible = false;
            pictureBox13.Visible = false;
            pictureBox13.Visible = false;
            pictureBox14.Visible = false;*/
        }

        private void pictureBox13_Click(object sender, EventArgs e)
        {
            imgescogida = new Bitmap(pictureBox13.Image);
            escogida += 1;
            dibujar_imagen(imgescogida);
            /*pictureBox2.Visible = false;
            pictureBox3.Visible = false;
            pictureBox4.Visible = false;
            pictureBox5.Visible = false;
            pictureBox6.Visible = false;
            pictureBox7.Visible = false;
            pictureBox8.Visible = false;
            pictureBox9.Visible = false;
            pictureBox10.Visible = false;
            pictureBox11.Visible = false;
            pictureBox12.Visible = false;
            pictureBox1.Visible = false;
            pictureBox14.Visible = false;*/
          
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            imgescogida = new Bitmap(pictureBox7.Image);
            escogida += 1;
            dibujar_imagen(imgescogida);
            /*pictureBox2.Visible = false;
            pictureBox3.Visible = false;
            pictureBox4.Visible = false;
            pictureBox5.Visible = false;
            pictureBox6.Visible = false;
            pictureBox1.Visible = false;
            pictureBox8.Visible = false;
            pictureBox9.Visible = false;
            pictureBox10.Visible = false;
            pictureBox11.Visible = false;
            pictureBox12.Visible = false;
            pictureBox13.Visible = false;
            pictureBox13.Visible = false;
            pictureBox14.Visible = false;*/
        }

        private void pictureBox9_Click(object sender, EventArgs e)
        {
            imgescogida = new Bitmap(pictureBox9.Image);
            escogida += 1;
            dibujar_imagen(imgescogida);
            /*pictureBox2.Visible = false;
            pictureBox3.Visible = false;
            pictureBox4.Visible = false;
            pictureBox5.Visible = false;
            pictureBox6.Visible = false;
            pictureBox7.Visible = false;
            pictureBox8.Visible = false;
            pictureBox1.Visible = false;
            pictureBox10.Visible = false;
            pictureBox11.Visible = false;
            pictureBox12.Visible = false;
            pictureBox13.Visible = false;
            pictureBox13.Visible = false;
            pictureBox14.Visible = false;*/
        }

        private void pictureBox10_Click(object sender, EventArgs e)
        {
            imgescogida = new Bitmap(pictureBox10.Image);
            escogida += 1;
            dibujar_imagen(imgescogida);
            /*pictureBox2.Visible = false;
            pictureBox3.Visible = false;
            pictureBox4.Visible = false;
            pictureBox5.Visible = false;
            pictureBox6.Visible = false;
            pictureBox7.Visible = false;
            pictureBox8.Visible = false;
            pictureBox9.Visible = false;
            pictureBox1.Visible = false;
            pictureBox11.Visible = false;
            pictureBox12.Visible = false;
            pictureBox13.Visible = false;
            pictureBox13.Visible = false;
            pictureBox14.Visible = false;*/
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            imgescogida = new Bitmap(pictureBox5.Image);
            escogida += 1;
            dibujar_imagen(imgescogida);
            /*pictureBox2.Visible = false;
            pictureBox3.Visible = false;
            pictureBox4.Visible = false;
            pictureBox1.Visible = false;
            pictureBox6.Visible = false;
            pictureBox7.Visible = false;
            pictureBox8.Visible = false;
            pictureBox9.Visible = false;
            pictureBox10.Visible = false;
            pictureBox11.Visible = false;
            pictureBox12.Visible = false;
            pictureBox13.Visible = false;
            pictureBox13.Visible = false;
            pictureBox14.Visible = false;*/
        }

        private void btnnext3_Click(object sender, EventArgs e)
        {
            if (escogida > 0 && escogida == 3)
            {
                this.Hide();
                Emoti ei = new Emoti();
                ei.Show();
            }
            else {
                label2.Visible = true;
                label2.Text = "Hacen falta emoticones o escogiste más de la cuenta";
            }
        }

       
    }
}
