﻿namespace kolbiinteractiva
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblnext = new System.Windows.Forms.Label();
            this.btnnext1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(309, 141);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(483, 198);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // lblnext
            // 
            this.lblnext.AutoSize = true;
            this.lblnext.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblnext.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(123)))));
            this.lblnext.Location = new System.Drawing.Point(346, 458);
            this.lblnext.Name = "lblnext";
            this.lblnext.Size = new System.Drawing.Size(670, 55);
            this.lblnext.TabIndex = 2;
            this.lblnext.Text = "Pulse siguiente para continuar";
            // 
            // btnnext1
            // 
            this.btnnext1.AutoSize = true;
            this.btnnext1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(147)))), ((int)(((byte)(142)))));
            this.btnnext1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnnext1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnnext1.ForeColor = System.Drawing.Color.White;
            this.btnnext1.Location = new System.Drawing.Point(942, 364);
            this.btnnext1.Name = "btnnext1";
            this.btnnext1.Size = new System.Drawing.Size(243, 78);
            this.btnnext1.TabIndex = 3;
            this.btnnext1.Text = "Siguiente";
            this.btnnext1.UseVisualStyleBackColor = false;
            this.btnnext1.Click += new System.EventHandler(this.btnnext1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(214)))), ((int)(((byte)(58)))));
            this.ClientSize = new System.Drawing.Size(1214, 532);
            this.Controls.Add(this.btnnext1);
            this.Controls.Add(this.lblnext);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblnext;
        private System.Windows.Forms.Button btnnext1;
    }
}

